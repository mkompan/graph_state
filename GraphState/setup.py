#!/usr/bin/python
# -*- coding: utf8
from setuptools import setup
from graph_state import __version__


setup(
    name='GraphState',
    version=__version__,
    author='D. Batkovich, S. Novikov, M.Kompaniets',
    author_email='batya239@gmail.com, dr.snov@gmail.com, mkompan@gmail.com',
    packages=['graph_state', 'nickel'],
    url='https://gitlab.com/mkompan/graph_state.git',
    license='LICENSE.txt',
    description='Graph library implementation using generalization of B.G.Nickel et al. algorithm for identifying graphs.',
    long_description=open('README.txt').read(),
)
