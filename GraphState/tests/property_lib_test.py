

import graph_state.property_lib as property_lib
import unittest


class TestFields(unittest.TestCase):
    def testCopy(self):
        fields = property_lib.Fields('ab')
        self.assertEqual(fields, fields.copy())
        swapped = fields.copy(swap=True)
        self.assertTrue(fields != swapped)
        self.assertEqual(fields, swapped.copy(swap=True))

    def testToFromString(self):
        fields = property_lib.Fields('ab')
        self.assertEqual(len(str(fields)), property_lib.Fields.STR_LEN)
        decoded = property_lib.Fields.from_str(str(fields))
        self.assertEqual(fields, decoded)

    def testFieldsToFromString(self):
        string = 'aBcD'
        fields = property_lib.Fields.fieldsFromStr(string)
        self.assertEqual(len(fields), 2)
        self.assertEqual(property_lib.Fields.fieldsToStr(fields),
                         string)

    def testHash(self):
        first = property_lib.Fields('ab')
        second = property_lib.Fields('ba').copy(swap=True)
        self.assertTrue(first == second)
        self.assertTrue(hash(first) == hash(second))


class TestRainbow(unittest.TestCase):
    def testToFromStr(self):
        r = property_lib.Rainbow((0, 1))
        self.assertEqual(str(r), '(0, 1)')
        self.assertEqual(r, property_lib.Rainbow.fromObject(str(r)))

    def testFromStr2(self):
        r = property_lib.Rainbow.fromObject("\"asd\"")
        self.assertEqual(r.colors, ("asd",))


class TestArrow(unittest.TestCase):
    def testInit(self):
        self.assertEqual("<", property_lib.Arrow('<')._value)
        self.assertEqual(">", property_lib.Arrow('>')._value)
        self.assertEqual("0", property_lib.Arrow('0')._value)
        self.assertRaises(AssertionError, property_lib.Arrow, 'q')

    def testValue(self):
        self.assertEqual("<", property_lib.Arrow('<').value)

    def testMakeExternal(self):
        pass

    def testIsNull(self):
        self.assertTrue(property_lib.Arrow("0").is_null())
        self.assertFalse(property_lib.Arrow('<').is_null())

    def testIsLeft(self):
        self.assertTrue(property_lib.Arrow("<").is_left())
        self.assertFalse(property_lib.Arrow('>').is_left())

#    def testIsRight(self):
#        self.assertTrue(property_lib.Arrow(">"))
#        self.assertFalse(property_lib.Arrow('<'))

    def testAsNumeric(self):
        self.assertEqual(1, property_lib.Arrow('<').as_numeric())
        self.assertEqual(-1, property_lib.Arrow('>').as_numeric())
        self.assertEqual(0, property_lib.Arrow('0').as_numeric())

    def testNeg(self):
        self.assertEqual(property_lib.Arrow('0'), -property_lib.Arrow('0'))
        self.assertEqual(property_lib.Arrow('<'), -property_lib.Arrow('>'))
        self.assertEqual(property_lib.Arrow('>'), -property_lib.Arrow('<'))

    def testEq(self):
        self.assertTrue(property_lib.Arrow('0') == property_lib.Arrow('0'))
        self.assertFalse(property_lib.Arrow('<') == property_lib.Arrow('>'))


class TestStringExternalizer(unittest.TestCase):
    def testDeserialize(self):
        self.assertIsNone(property_lib.StringExternalizer().deserialize("None"))
        self.assertEqual("123", property_lib.StringExternalizer().deserialize("123"))
        self.assertEqual("123", property_lib.StringExternalizer().deserialize(123))


if __name__ == "__main__":
    unittest.main()
