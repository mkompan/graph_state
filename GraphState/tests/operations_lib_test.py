#!/usr/bin/python
# -*- coding: utf8 -*-
#
# Test for operations_lib.py. Provides test cases for useful util functions like determination of connected components,
# irreducibility etc.
#

__author__ = 'dima'


import unittest
import graph_state.operations_lib as operations_lib
import graph_state.property_lib as property_lib
import graph_state.graph_state as graph_state
import graph_state.graph_state_property as graph_state_property


config = property_lib.COLORS_AND_FIELDS_CONFIG


class OperationsLibTest(unittest.TestCase):
    def testEdgesForNode(self):
        gs = config.graph_state_from_str("e1|e|")
        self.assertEqual([graph_state.Edge, graph_state.Edge], list(map(type, operations_lib.edges_for_node(gs, 0))))
        self.assertEqual([(-1, 0), (0, 1)], [e.nodes for e in operations_lib.edges_for_node(gs, 0)])
        gs = config.graph_state_from_str("e1|e|")
        self.assertEqual([], [e.nodes for e in operations_lib.edges_for_node(gs, 3)])

    def testSplitEdgesForNode(self):
        gs = config.graph_state_from_str("e01234|||||")
        wn, won = operations_lib.split_edges_for_node(gs, 0)
        self.assertEqual([(-1, 0), (0, 0), (0, 1), (0, 2), (0, 3), (0, 4)], [e.nodes for e in wn])
        self.assertEqual([], [e.nodes for e in won])
        gs = config.graph_state_from_str("e01234|||||")
        wn, won = operations_lib.split_edges_for_node(gs, 6)
        self.assertEqual([], [e.nodes for e in wn])
        self.assertEqual([(-1, 0), (0, 0), (0, 1), (0, 2), (0, 3), (0, 4)], [e.nodes for e in won])
        gs = config.graph_state_from_str("e01234|||||")
        wn, won = operations_lib.split_edges_for_node(gs, 2)
        self.assertEqual([(0, 2)], [e.nodes for e in wn])
        self.assertEqual([(-1, 0), (0, 0), (0, 1), (0, 3), (0, 4)], [e.nodes for e in won])

    def testGetExternalNode(self):
        gs = config.graph_state_from_str("e11|e|")
        self.assertEqual(-1, operations_lib.get_external_node(gs))
        self.assertEqual(graph_state_property.Node(-1, object()), operations_lib.get_external_node(gs))

    def testGetBoundVertices(self):
        gs = config.graph_state_from_str("e1|e|")
        self.assertEqual({0, 1}, operations_lib.get_bound_vertices(gs))
        self.assertEqual({0, 1}, operations_lib.get_bound_vertices(gs.edges))
        gs = config.graph_state_from_str("11||")
        self.assertEqual(set(), operations_lib.get_bound_vertices(gs))

    def testGetVertices(self):
        gs = config.graph_state_from_str("e12|e2|e|")
        v = operations_lib.get_vertices(gs)
        self.assertEqual([graph_state.Node, graph_state.Node, graph_state.Node, graph_state.Node], [type(x) for x in v])
        self.assertEqual(frozenset([-1, 0, 1, 2]), v)

    def testGetConnectedComponents(self):
        edges = [config.new_edge(nodes) for nodes in [(-1, 0), (0, 1), (1, 2), (2, 3), (3, 4), (4, -1)]]
        components = operations_lib.get_connected_components(edges)
        self.assertEqual(len(components), 1)
        self.assertEqual(set(components[0]), set([0, 1, 2, 3, 4]))

        edges = [config.new_edge(nodes) for nodes in [(-1, 0), (0, 1), (1, 2), (3, 4), (4, -1)]]
        components = operations_lib.get_connected_components(edges)
        self.assertEqual(len(components), 2)
        self.assertEqual(set(components[0]), set([0, 1, 2]))
        self.assertEqual(set(components[1]), set([3, 4]))

        edges = [config.new_edge(nodes) for nodes in [(-1, 0), (0, 1), (1, 2), (2, 3), (3, 4), (4, -1)]]
        components = operations_lib.get_connected_components(edges, additional_vertices=[4, 5])
        self.assertEqual(len(components), 2)

        components = operations_lib.get_connected_components(edges, singular_vertices=set([2]))
        self.assertEqual(len(components), 2)

    def testIsEdgePropertyIsFullyNone(self):
        edges = [config.new_edge(nodes) for nodes in [(-1, 0), (0, 1), (1, 2), (2, 3), (3, 4), (4, -1)]]
        self.assertTrue(operations_lib.is_edge_property_fully_none(edges, "colors"))
        edges[3] = edges[3].copy(colors=(2, 3))
        self.assertFalse(operations_lib.is_edge_property_fully_none(edges, "colors"))

    def testIsGraphConnected(self):
        edges = [config.new_edge(nodes) for nodes in [(-1, 0), (0, 1), (1, -1)]]
        self.assertTrue(operations_lib.is_graph_connected(edges))
        edges = [config.new_edge(nodes) for nodes in [(-1, 0), (0, 1), (2, -1)]]
        self.assertFalse(operations_lib.is_graph_connected(edges))
        edges.append(config.new_edge((1, 2)))
        self.assertTrue(operations_lib.is_graph_connected(edges))

    def testIsVertexIrreducible(self):
        gs = config.graph_state_from_str("e11|22|e|")
        self.assertFalse(operations_lib.is_vertex_irreducible(gs))

        gs = config.graph_state_from_str("e12|23|3|e|")
        self.assertTrue(operations_lib.is_vertex_irreducible(gs))

    def testIsOneIrreducible(self):
        gs = config.graph_state_from_str("e11|22|e|")
        self.assertTrue(operations_lib.is_1_irreducible(gs))

        gs = config.graph_state_from_str("e1|e|")
        self.assertFalse(operations_lib.is_1_irreducible(gs))


class DisjointSetTest(unittest.TestCase):
    def testInit(self):
        ds = operations_lib.DisjointSet()
        self.assertEqual(dict(), ds.underlying)
        ds2 = operations_lib.DisjointSet(keys={1, 2, 3})
        self.assertEqual({1: 1, 2: 2, 3: 3}, ds2.underlying)

    def testAddKey(self):
        ds = operations_lib.DisjointSet()
        ds.add_key(1)
        self.assertEqual({1: 1}, ds.underlying)
        ds.underlying[1] = 10
        ds.add_key(1)
        self.assertEqual({1: 10}, ds.underlying)

    def testRoot(self):
        ds = operations_lib.DisjointSet(keys={1, 2, 3})
        self.assertEqual(1, ds.root(1))
        self.assertEqual(2, ds.root(2))
        self.assertEqual(3, ds.root(3))

        ds.union([1, 2])
        self.assertEqual(2, ds.root(1))
        self.assertEqual(2, ds.root(2))
        self.assertEqual(3, ds.root(3))

    def testUnion(self):
        ds = operations_lib.DisjointSet()
        ds.union([1, 2])        # both absent
        self.assertEqual({1: 2, 2: 2}, ds.underlying)
        ds.union([2, 3])        # second absent
        self.assertEqual({1: 2, 2: 3, 3: 3}, ds.underlying)
        ds.union([3, 1])  # both present
        self.assertEqual({1: 2, 2: 3, 3: 3}, ds.underlying)
        ds.union([4, 1])  # both present
        self.assertEqual({1: 2, 2: 3, 3: 3, 4: 3}, ds.underlying)

    def testGetConnectedComponents(self):
        ds = operations_lib.DisjointSet()
        self.assertEqual([], ds.get_connected_components())

        ds = operations_lib.DisjointSet(keys={1, 2, 3, 4})
        self.assertEqual([[1], [2], [3], [4]], ds.get_connected_components())
        ds = operations_lib.DisjointSet(keys={1, 2, 3, 4})
        ds.union([1, 2])
        self.assertEqual([[1, 2], [3], [4]], ds.get_connected_components())

    def testNextSingularKey(self):
        ds = operations_lib.DisjointSet()
        self.assertEqual('__1_1', ds.next_singular_key(1))
        self.assertEqual('__2_1', ds.next_singular_key(1))
        self.assertEqual('__3_1', ds.next_singular_key(1))
        self.assertEqual('__4_2', ds.next_singular_key(2))

    def testIsSingular(self):
        self.assertEqual(True, operations_lib.DisjointSet.is_singular('__1_1'))
        self.assertEqual(False, operations_lib.DisjointSet.is_singular(3))

    def testOverall(self):
        ds = operations_lib.DisjointSet()
        ds.union([1, ds.next_singular_key(2)])
        ds.union([ds.next_singular_key(2), 3])
        ds.union([3, 4])
        ds.union([4, ds.next_singular_key(2)])
        self.assertEqual([[1, '__1_2'], ['__2_2', 3, 4, '__3_2']], ds.get_connected_components())


if __name__ == "__main__":
    unittest.main()