#!/usr/bin/python
# -*- coding: utf8
from setuptools import setup
from gtopology import __version__


setup(
    name='GTopology',
    version=__version__,
    author='S. Novikov',
    author_email='dr.snov@gmail.com',
    packages=['gtopology'],
    url='https://gitlab.com/mkompan/graph_state.git',
    license='LICENSE.txt',
    description='Graph "topology" generation on top of GraphState',
    long_description=open('README.txt').read(),
)
