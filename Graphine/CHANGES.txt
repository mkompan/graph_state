v 1.0.0 - initial package

v 1.0.0 - depends only on GraphState

v 1.0.2 - added doc strings

v 1.0.3  03/03/2015 - README.txt

v 1.0.4  03/03/2015 - README.txt

v 1.0.5  10/03/2015 - docs

v 1.0.8  22/03/2016 - subgraph can contain only one edge
                    - cut_edges_to_external parameter in x_sub_graphs and x_relevant_subgraphs marked as depricated.
