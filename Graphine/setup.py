#!/usr/bin/python
# -*- coding: utf8
# from distutils.core import setup
from setuptools import setup
from graphine import __version__

setup(
    name='PyGraphine',
    version=__version__,
    author='D. Batkovich, M. Kompaniets',
    author_email='batya239@gmail.com, mkompan@gmail.com',
    packages=['graphine'],
    url='https://gitlab.com/mkompan/graph_state.git',
    license='LICENSE.txt',
    description='Graph manipulation package based on GraphState',
    long_description=open('README.txt').read(),
    install_requires='GraphState >= 1.0.9'
)
